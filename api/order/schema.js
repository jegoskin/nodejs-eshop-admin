const mongoose = require('mongoose');

const UserSchema = mongoose.Schema({
	firstName: {
		type: String,
		required: true,
		min: 3,
		max: 255
	},
	lastName: {
		type: String,
		required: true,
		min: 3,
		max: 255
	},
	isRegistered: {
		type: Boolean,
	},
	telephone: {
		type: String,
		min: 3,
		max: 255
	},
	email: {
		type: String,
		required: true,
		min: 3,
		max: 255
	},
	invoiceCompanyName: {
		type: String,
		min: 3,
		max: 255
	},
	invoiceCompanyCin: {
		type: String,
		min: 3,
		max: 255
	},
	invoiceCompanyVatin: {
		type: String,
		min: 3,
		max: 255
	},
	invoiceStreet: {
		type: String,
		min: 3,
		max: 255
	},
	invoiceZipCode: {
		type: String,
		min: 3,
		max: 255
	},
	invoiceTown: {
		type: String,
		min: 3,
		max: 255
	},
	invoiceCountry: {
		type: String,
		min: 3,
		max: 255
	},

	shippingFirstName: {
		type: String,
		min: 3,
		max: 255
	},	
	shippingLastName: {
		type: String,
		min: 3,
		max: 255
	},
	shippingCompanyName: {
		type: String,
		min: 3,
		max: 255
	},
	shippingStreet: {
		type: String,
		min: 3,
		max: 255
	},
	shippingZipCode: {
		type: String,
		min: 3,
		max: 255
	},
	shippingTown: {
		type: String,
		min: 3,
		max: 255
	},
	shippingCountry: {
		type: String,
		min: 3,
		max: 255
	},
});

const orderSchema = new mongoose.Schema({
  user: {
		type: UserSchema,
		required: true
	},
	date: {
		type: Date,
		default: Date.now,
	}
});

module.exports = mongoose.model('Order', orderSchema);