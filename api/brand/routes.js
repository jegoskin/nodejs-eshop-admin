const router = require('express').Router();
const Brand = require('./schema');
const log = require('../../services/log.service');

router.get('/', async (req, res) => {
	try {
		const data = await Brand.find();
		res.json( data )
	} catch(err) {
		log.err(err);
		res.json({ message: err })
	}
});

router.post('/', async (req, res) => {
	const post = new Brand({
		name: req.body.name
	});

	try {
		const savedBrand = await post.save();
		res.json(savedBrand);
	} catch (err) {
		log.err(err);
		res.json({ message: err });
	}
});

router.get('/:brandId', async (req, res) => {
	try {
		const post = await Brand.findById(req.params.brandId);
		res.json(post);
	} catch(err) {
		log.err(err);
		res.json({ message: err });
	}
});

router.delete('/:brandId', async (req, res) => {
	try {
		const removeBrand = await Brand.deleteOne({ _id: req.params.brandId });
		res.json(removeBrand)
	} catch (err) {
		log.err(err);
		res.json({ message: err });
	}
});

router.put('/:brandId', async (req, res) => {
	try {
		const updatedBrand = await Brand.updateOne(
			{ _id: req.params.brandId }, 
			{ $set: { name: req.body.name }}
		);
		res.json(updatedBrand)
	} catch (err) {
		log.err(err);
		res.json({ message: err });
	}
});

module.exports = router;