const router = require('express').Router();
const Post = require('./schema');
const pagination = require('../../lib/pagination');
const log = require('../../services/log.service');

router.get('/', async (req, res) => {
	try {
		const { pageIndex, pageSize, sort, find } = pagination(req.query);

		const count = await Post.countDocuments(find);
		const data = await Post.find(find).sort(sort).skip(pageSize * pageIndex).limit(pageSize);

		res.json({ data, pagination: { count, pageIndex, pageSize } })
	} catch(err) {
		log.err(err);
		res.json({ message: err })
	}
});

router.post('/', async (req, res) => {
	const post = new Post({
		title: req.body.title,
		description: req.body.description
	});

	try {
		const savedPost = await post.save();
		res.json(savedPost);
	} catch (err) {
		log.err(err);
		res.json({ message: err });
	}
});

router.get('/:postId', async (req, res) => {
	try {
		const post = await Post.findById(req.params.postId);
		res.json(post);
	} catch(err) {
		log.err(err);
		res.json({ message: err });
	}
});

router.delete('/:postId', async (req, res) => {
	try {
		const removePost = await Post.deleteOne({ _id: req.params.postId });
		res.json(removePost)
	} catch (err) {
		log.err(err);
		res.json({ message: err });
	}
});

router.put('/:postId', async (req, res) => {
	try {
		const updatedPost = await Post.updateOne(
			{ _id: req.params.postId }, 
			{ $set: { title: req.body.title, description: req.body.description }}
		);
		res.json(updatedPost)
	} catch (err) {
		log.err(err);
		res.json({ message: err });
	}
});

module.exports = router;