class Logger {
	static info(message) {
		console.log(`[${Logger.time()}] ${message}`);
	}

	static err(err) {
		console.error(`[${Logger.time()}] ${err.name}: ${err.message}\n${err.stack}`);
	}

	static time() {
		return new Date().toLocaleString(undefined, { year: 'numeric', month: '2-digit', day: '2-digit', hour: '2-digit', minute: '2-digit', second: '2-digit' });
	}
}

module.exports = Logger;
