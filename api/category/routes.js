const router = require('express').Router();
const Category = require('./schema');
const log = require('../../services/log.service');

router.get('/', async (_, res) => {
	try {
		const data = await Category.find();
		res.json( data )
	} catch(err) {
		log.err(err);
		res.json({ message: err })
	}
});

router.post('/', async (req, res) => {
	const post = new Category({
		name: req.body.name
	});

	try {
		const savedCategory = await post.save();
		res.json(savedCategory);
	} catch (err) {
		log.err(err);
		res.json({ message: err });
	}
});

router.get('/:categoryId', async (req, res) => {
	try {
		const post = await Category.findById(req.params.categoryId);
		res.json(post);
	} catch(err) {
		log.err(err);
		res.json({ message: err });
	}
});

router.delete('/:categoryId', async (req, res) => {
	try {
		const removeCategory = await Category.deleteOne({ _id: req.params.categoryId });
		res.json(removeCategory);
	} catch (err) {
		log.err(err);
		res.json({ message: err });
	}
});

router.put('/:categoryId', async (req, res) => {
	try {
		const updatedCategory = await Category.updateOne(
			{ _id: req.params.categoryId }, 
			{ $set: { name: req.body.name }}
		);
		res.json(updatedCategory);
	} catch (err) {
		log.err(err);
		res.json({ message: err });
	}
});

module.exports = router;