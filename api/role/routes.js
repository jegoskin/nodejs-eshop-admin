const router = require('express').Router();
const Role = require('./schema');
const log = require('../../services/log.service');

router.get('/', async (_, res) => {
	try {
		const data = await Role.find();
		res.json( data )
	} catch(err) {
		log.err(err);
		res.json({ message: err })
	}
});

module.exports = router;