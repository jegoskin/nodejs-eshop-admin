const router = require('express').Router();
const User = require('./schema');
const pagination = require('../../lib/pagination');
const log = require('../../services/log.service');
const bcrypt = require('bcryptjs');

router.get('/', async (req, res) => {
	try {
		if (req.query.pageIndex !== undefined) {
			const { pageIndex, pageSize, sort, find } = pagination(req.query);
			const count = await User.countDocuments(find);
			let data = await User.find(find).sort(sort).skip(pageSize * pageIndex).limit(pageSize);
			delete data.password;
			res.json({ data, pagination: { count, pageIndex, pageSize } });
		} else {
			let data = await User.find().sort({ 'lastName' : -1 });
			delete data.password;
			res.json( data );
		}
	} catch(err) {
		log.err(err);
		res.json({ message: err })
	}
});

router.post('/', async (req, res) => {
	const post = new User({
		name: req.body.name,
		lastName: req.body.lastName,
		email: req.body.email,
		telephone: req.body.telephone,
		password: req.body.password,
		role: req.body.role
	});

	try {
		const savedUser = await post.save();
		res.json(savedUser);
	} catch (err) {
		log.err(err);
		res.json({ message: err });
	}
});

router.post('/password/:userId', async (req, res) => {
	try {
	const salt = await bcrypt.genSalt(10);
	const hashPassword = await bcrypt.hash(req.body.password, salt);

		const updatedUser = await User.updateOne(
			{ _id: req.params.userId },
			{ $set: {
				password: hashPassword,
				}
			}
		);
		res.json(updatedUser);
	} catch (err) {
		log.err(err);
		res.json({ message: err });
	}
});

router.get('/:userId', async (req, res) => {
	try {
		const post = await User.findById(req.params.userId);
		res.json(post);
	} catch(err) {
		log.err(err);
		res.json({ message: err });
	}
});

router.delete('/:userId', async (req, res) => {
	try {
		const removePost = await User.deleteOne({ _id: req.params.userId });
		res.json(removePost)
	} catch (err) {
		log.err(err);
		res.json({ message: err });
	}
});

router.put('/:userId', async (req, res) => {
	try {
		const updatedUser = await User.updateOne(
			{ _id: req.params.userId },
			{ $set: {
				firstName: req.body.firstName,
				lastName: req.body.lastName,
				email: req.body.email,
				telephone: req.body.telephone,
				role: req.body.role,

				invoiceCompanyName: req.body.invoiceCompanyName,
				invoiceCompanyCin: req.body.invoiceCompanyCin,
				invoiceCompanyVatin: req.body.invoiceCompanyVatin,
				invoiceStreet: req.body.invoiceStreet,
				invoiceZipCode: req.body.invoiceZipCode,
				invoiceTown: req.body.invoiceTown,
				invoiceCountry: req.body.invoiceCountry,

				shippingFirstName: req.body.shippingFirstName,
				shippingLastName: req.body.shippingLastName,
				shippingCompanyName: req.body.shippingCompanyName,
				shippingStreet: req.body.shippingStreet,
				shippingZipCode: req.body.shippingZipCode,
				shippingTown: req.body.shippingTown,
				shippingCountry: req.body.shippingCountry,

				modifyDate: Date.now(),
				}
			}
		);
		res.json(updatedUser);
	} catch (err) {
		log.err(err);
		res.json({ message: err });
	}
});

module.exports = router;