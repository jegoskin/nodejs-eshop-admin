const jwt = require('jsonwebtoken');

class TokenManager {
  generate(userId) {
    const refreshId = Date.now();

    const refreshToken = jwt.sign(
    	{
    		refreshId: refreshId,
    		userId: userId
    	},
    	process.env.TOKEN_SECRET, 
    	{ expiresIn: '30d' }
    );

    const accessToken = jwt.sign(
    	{ 
    		userId: userId,
    	},
    	process.env.TOKEN_SECRET, 
    	{ expiresIn: '20m' }
    );

    return {
      refreshToken,
      accessToken,
      refreshId
    }
  }
}

const tokenManager = new TokenManager();

module.exports = tokenManager;