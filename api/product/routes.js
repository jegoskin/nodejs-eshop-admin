const router = require('express').Router();
const Product = require('./schema');
const mongoose = require('mongoose');
const Quantity = require('../quantity/schema');
// const pagination = require('../../lib/pagination');
const log = require('../../services/log.service');

router.get('/', async (req, res) => {
	try {
    // console.log(req.query);
    const pageIndex = parseInt(req.query.pageIndex);
    const pageSize = parseInt(req.query.pageSize);
  
    let sort = new Object();
    sort[req.query.sorterField] = req.query.sorterOrder === 'descend'? '-1' : '1';
  
    let find = new Object();
    if (req.query.filterString !== 'undefined') {
      find[req.query.filterField] = new RegExp(req.query.filterString.replace(',','|'), 'i');
    } else {
      find = {}
    }

    // console.log(find);

    const count = await Product.countDocuments(find);

    const data = await Product
      .find(find)
      .sort(sort)
      .skip(pageSize * pageIndex)
      .limit(pageSize)
      .populate('brand', 'name')
      .populate('category', 'name')

		res.json({ data, pagination: { count, pageIndex, pageSize } })
	} catch(err) {
		log.err(err);
		res.json({ message: err })
	}
});

router.post('/', async (req, res) => {
	const product = new Product({
		name: req.body.name,
    category: req.body.categoryId,
		brand: req.body.brandId,
		price: req.body.price
	});

	try {
		const savedProduct = await product.save();
		res.json(savedProduct);
	} catch (err) {
		log.err(err);
		res.json({ message: err });
	}
});

router.get('/:productId', async (req, res) => {
	try {
		const product = await Product.findById(req.params.productId);
		res.json(product);
	} catch(err) {
		log.err(err);
		res.json({ message: err });
	}
});

router.delete('/:productId', async (req, res) => {
	try {
		const removeProduct = await Product.deleteOne({ _id: req.params.productId });
		res.json(removeProduct)
	} catch (err) {
		log.err(err);
		res.json({ message: err });
	}
});

router.put('/:productId', async (req, res) => {
	try {
		const updatedProduct = await Product.updateOne(
			{ _id: req.params.productId },
      { $set:
        {
          name: req.body.name,
          brand: req.body.brandId,
          category: req.body.categoryId,
          price: req.body.price
        }
      }
		);

		Object
			.entries(req.body.size)
			.forEach(async (key) => {
				const findExistQuantity = await Quantity.find({ product: req.params.productId, size: key[0] });

				if (!findExistQuantity.length) {
					const newQuantitySize = new Quantity({
						price: key[1].price,
						quantity: key[1].quantity,
						product: req.params.productId,
						size: key[0]
					});

					try {
						await newQuantitySize.save();
					} catch (err) {
						log.err(err);
						res.json({ message: err });
					}
				} else {
					try {
						await Quantity.updateOne(
							{ product: req.params.productId, size: key[0] },
							{ $set:
								{
									price: key[1].price,
									quantity: key[1].quantity,
									product: req.params.productId,
									size: key[0]
								}
							}
						);
					} catch (err) {
						log.err(err);
						res.json({ message: err });
					}
				}
			});
		res.json(updatedProduct);
	} catch (err) {
		log.err(err);
		res.json({ message: err });
	}
});

module.exports = router;