const mongoose = require('mongoose');

const brandSchema = new mongoose.Schema({
	name: {
		type: String,
		required: true,
	},
	date: {
		type: Date,
		default: Date.now,
	}
});

module.exports = mongoose.model('Brand', brandSchema);