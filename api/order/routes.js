const router = require('express').Router();
const Order = require('./schema');
const log = require('../../services/log.service');

router.get('/', async (req, res) => {
	try {
    const pageIndex = parseInt(req.query.pageIndex);
    const pageSize = parseInt(req.query.pageSize);
  
    let sort = new Object();
    sort[req.query.sorterField] = req.query.sorterOrder === 'descend'? '-1' : '1';
  
    let find = new Object();
    if (req.query.filterString !== 'undefined') {
      find[req.query.filterField] = new RegExp(req.query.filterString.replace(',','|'), 'i');
    } else {
      find = {}
    }

    const count = await Order.countDocuments(find);

    const data = await Order
      .find(find)
      .sort(sort)
      .skip(pageSize * pageIndex)
      .limit(pageSize)

		res.json({ data, pagination: { count, pageIndex, pageSize } })
	} catch(err) {
		log.err(err);
		res.json({ message: err })
	}
});

router.post('/', async (req, res) => {
	const order = new Order({ ...req.body });
	try {
		const savedOrder = await order.save();
		res.json(savedOrder);
	} catch (err) {
		log.err(err);
		res.json({ message: err });
	}
});

router.get('/:orderId', async (req, res) => {
	try {
		console.log(req.params);
		const order = await Order.findById(req.params.orderId);
		res.json(order);
	} catch(err) {
		log.err(err);
		res.json({ message: err });
	}
});

router.delete('/:orderId', async (req, res) => {
	try {
		const removeOrder = await Order.deleteOne({ _id: req.params.orderId });
		res.json(removeOrder);
	} catch (err) {
		log.err(err);
		res.json({ message: err });
	}
});

router.put('/:orderId', async (req, res) => {
	try {
		console.log(req.body);
		const updatedOrder = await Order.updateOne(
			{ _id: req.params.orderId },
			{ $set: { ...req.body }}
		);
		res.json(updatedOrder);
	} catch (err) {
		log.err(err);
		res.json({ message: err });
	}
});

module.exports = router;