const router = require('express').Router();
const Size = require('./schema');
const log = require('../../services/log.service');

router.get('/', async (_, res) => {
	try {
		const data = await Size.find();
		res.json( data )
	} catch(err) {
		log.err(err);
		res.json({ message: err })
	}
});

router.post('/', async (req, res) => {
	const post = new Size({
		name: req.body.name
	});

	try {
		const savedSize = await post.save();
		res.json(savedSize);
	} catch (err) {
		log.err(err);
		res.json({ message: err });
	}
});

router.get('/:sizeId', async (req, res) => {
	try {
		const post = await Size.findById(req.params.sizeId);
		res.json(post);
	} catch(err) {
		log.err(err);
		res.json({ message: err });
	}
});

router.delete('/:sizeId', async (req, res) => {
	try {
		const removeSize = await Size.deleteOne({ _id: req.params.sizeId });
		res.json(removeSize)
	} catch (err) {
		log.err(err);
		res.json({ message: err });
	}
});

router.put('/:sizeId', async (req, res) => {
	try {
		const updatedSize = await Size.updateOne(
			{ _id: req.params.sizeId }, 
			{ $set: { name: req.body.name }}
		);
		res.json(updatedSize)
	} catch (err) {
		log.err(err);
		res.json({ message: err });
	}
});

module.exports = router;