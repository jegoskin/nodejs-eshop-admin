const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({
	name: {
		type: String,
		required: true,
	},
	brand: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'Brand'
	},
  category: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'Category'
  },
  price: {
		type: Number,
		required: true,
	},
	date: {
		type: Date,
		default: Date.now,
	}
});

module.exports = mongoose.model('Product', productSchema);