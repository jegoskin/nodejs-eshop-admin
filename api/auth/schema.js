const mongoose = require('mongoose');

const refreshToken = new mongoose.Schema({
	refreshId: {
		type: Number,
		required: true
	},
	userId: {
		type: String,
		required: true,
		min: 6,
		max: 255
	},
	expire: {
    type: Date,
		required: true
	},
	create: {
    type: Date,
		default: Date.now
	}
});

module.exports = mongoose.model('RefreshToken', refreshToken);