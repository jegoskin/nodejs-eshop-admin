const router = require('express').Router();
const Quantity = require('./schema');
const log = require('../../services/log.service');

router.get('/:productId', async (req, res) => {
  try {
    const quantities = await Quantity.find({ product: req.params.productId });
    res.json(quantities);
  } catch(err) {
    log.err(err);
    res.json({ message: err });
  }
});

module.exports = router;