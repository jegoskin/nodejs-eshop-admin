const mongoose = require('mongoose');

const quantitySchema = new mongoose.Schema({
	product: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'Product'
	},
  size: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'Size'
  },
  quantity: {
		type: Number,
		required: true,
	},
  price: {
		type: Number,
		required: true,
	},
	date: {
		type: Date,
		default: Date.now,
	}
});

module.exports = mongoose.model('Quantity', quantitySchema);