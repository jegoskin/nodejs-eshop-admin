const express = require('express');
const app = express();
const cors = require('cors');
const dotenv = require('dotenv');
const mongoose = require('mongoose');
const cookieParser = require('cookie-parser');
const log = require('./services/log.service');
const auth = require('./services/verifyToken.service');

//Import Routes
const authRoute = require('./api/auth/routes');
const postRoute = require('./api/post/routes');
const userRoute = require('./api/user/routes');
const brandRoute = require('./api/brand/routes');
const quantityRoute = require('./api/quantity/routes');
const productRoute = require('./api/product/routes');
const categoryRoute = require('./api/category/routes');
const sizeRoute = require('./api/size/routes');
const orderRoute = require('./api/order/routes');
const roleRoute = require('./api/role/routes');

dotenv.config();

app.use(cors({
	credentials: true,
	origin: (origin, cb) => {
		if (!origin) return cb(null, true);

		if (process.env.ALLOW_ORIGINS.indexOf(origin) === -1){
      var msg = 'The CORS policy for this site does not ' +
                'allow access from the specified Origin.';
      return cb(new Error(msg), false);
		}
		return cb(null, true);
	},
}));

//Connect to DB
(async () => {
	try {
		await mongoose.connect(
			process.env.DB_CONNECT,
			{ useNewUrlParser: true, useUnifiedTopology: true },
		);
		log.info('Connected to db - OK');
	} catch (err) {
		log.err(err);
		process.exit(1);
	}
})();

//Middleware
app.use(express.json());
app.use(cookieParser());

//Route Middlewares
app.use('/api/user', authRoute);
app.use(auth);
app.use('/api/post', postRoute);
app.use('/api/product', productRoute);
app.use('/api/brand', brandRoute);
app.use('/api/quantity', quantityRoute);
app.use('/api/category', categoryRoute);
app.use('/api/size', sizeRoute);
app.use('/api/order', orderRoute);
app.use('/api/user', userRoute);
app.use('/api/role', roleRoute);

app.listen( process.env.PORT, 
	(err) => {
		if (err) {
			log.err(err);
		} else {
			log.info(`Server start ${process.env.PORT} - OK`);
		}
	}
);