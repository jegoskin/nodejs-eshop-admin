const jwt = require('jsonwebtoken');
const log = require('../services/log.service');
const RefreshToken = require('../api/auth/schema');
const User = require('../api/user/schema');
const tokenManager = require('./TokenManager');

async function auth(req, res, next)  {
	const existAccessToken = req.cookies.access_token;
	const existRefreshToken = req.cookies.refresh_token;

	if (existAccessToken) {
		try {
			jwt.verify(existAccessToken, process.env.TOKEN_SECRET);
			log.info(`Access Token exist`);
			next();
		} catch (err) {
			log.err(err);
			res.status(400).send('Invalid Token');
		}
	} else if(!existRefreshToken) {
		return res.status(401).send('Access Denied');
	} else {
		try {
			const verified = jwt.verify(existRefreshToken, process.env.TOKEN_SECRET);

			//check exist refresh token
			const refreshTokenData = await RefreshToken.findOne({ refreshId: verified.refreshId });
			
			if (!refreshTokenData || (new Date(refreshTokenData.expire) <= new Date(Date.now()))) {
				return res.status(400).send('Token not found or expired')
			}

			//check exist user
			const user = await User.findOne({ _id: refreshTokenData.userId });
			if (!user) {
				return res.status(400).send('User not found')
			}

			// create and assign token
			const { refreshToken, accessToken, refreshId } = tokenManager.generate(user._id);

			const saveRefreshToken = new RefreshToken({
				refreshId: refreshId,
				userId: user._id,
				expire: Date.now() + (60 * 60 * 24 * 30 * 1000)
			});

			try {
				await saveRefreshToken.save();
			} catch (err) {
				res.status(400).send(err);
			};

			res.cookie('access_token', accessToken, { maxAge: (60 * 20 * 1000), httpOnly: true });
			res.cookie('refresh_token', refreshToken, { maxAge: (60 * 60 * 24 * 30 * 1000), httpOnly: true });

			log.info(`Refresh Token exist`);
			next();

		} catch (err) {
			log.err(err);
			res.status(400).send('Invalid Token');
		}
	}
}

module.exports = auth;