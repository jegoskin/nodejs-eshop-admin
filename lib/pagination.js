function pagination(query) {
  const pageIndex = parseInt(query.pageIndex);
  const pageSize = parseInt(query.pageSize);

  let sort = new Object();
  sort[query.sorterField] = query.sorterOrder === 'descend'? '-1' : '1';

  let find = new Object();
  if (query.filterString !== 'undefined') {
    find[query.filterField] = new RegExp(query.filterString.replace(',','|'), 'i');
  } else {
    find = {}
  }

  return { pageIndex, pageSize, sort, find }
}

module.exports = pagination;


//find[query.filterField] = BranID : {$in:  [ObjectId(''),ObjectId('') ]};
// { brand: { $in : [ObjectId('5e413f7c6a8a323338148085')] } }

// const a = [
//   456,
//   457
// ]

// console.log(a.split(','))