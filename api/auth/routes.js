const router = require('express').Router();
const User = require('../user/schema');
const RefreshToken = require('./schema');
const bcrypt = require('bcryptjs');
const log = require('../../services/log.service');
const jwt = require('jsonwebtoken');
const { registerValidation, loginValidation } = require('../../validation');
const tokenManager = require('../../services/TokenManager');

router.post('/register', async (req, res) => {
	//validation
	const { error } = registerValidation(req.body)

	if (error) {
		res.status(400).send(error.details[0].message)
	}

	//check exist user
	const emailExist = await User.findOne({ email: req.body.email });
	if (emailExist) {
		return res.status(400).send('Email already exist')
	}

	//hash password
	const salt = await bcrypt.genSalt(10);
	const hashPassword = await bcrypt.hash(req.body.password, salt);

	//create a new user
	const user = new User({
		firstName: req.body.firstName,
		email: req.body.email,
		password: hashPassword
	});

	try {
		await user.save();
		res.send({ user: user._id });
	} catch (err) {
		res.status(400).send(err);
	}

});

router.post('/init', async (req, res) => {
	//validation
	const { error } = registerValidation(req.body)


	
	try {
		const count = await User.countDocuments(find);
		console.log(count);
	} catch(err) {
		log.err(err);
		res.json({ message: err })
	}




	if (error) {
		res.status(400).send(error.details[0].message)
	}

	//check exist user
	const emailExist = await User.findOne({ email: req.body.email });
	if (emailExist) {
		return res.status(400).send('Email already exist')
	}

	//hash password
	const salt = await bcrypt.genSalt(10);
	const hashPassword = await bcrypt.hash(req.body.password, salt);

	//create a new user
	const user = new User({
		firstName: req.body.firstName,
		email: req.body.email,
		password: hashPassword
	});

	try {
		// await user.save();
		// res.send({ user: user._id });
	} catch (err) {
		res.status(400).send(err);
	}

});

router.post('/refresh', async (req, res) => {
	const existAccessToken = req.cookies.access_token;
	const existRefreshToken = req.cookies.refresh_token;

	if (existAccessToken) {
		try {
			const verifiedAccessToken = jwt.verify(existAccessToken, process.env.TOKEN_SECRET);

			//check exist user
			const user = await User.findOne({ _id: verifiedAccessToken.userId });
			if (!user) {
				return res.status(400).send('User not found')
			}

			const response = {
				id: user._id,
				firstName: user.firstName,
				lastName: user.lastName,
				email: user.email,
				role: user.role,
			}

			log.info(`Refresh - Access Token exist`);
			res.send(response);
		} catch (err) {
			log.err(err);
			res.status(400).send('Invalid Token');
		}

	} else if(!existRefreshToken) {
		return res.status(401).send('Access Denied');
	} else {
		try {
			const verified = jwt.verify(existRefreshToken, process.env.TOKEN_SECRET);

			//check exist refresh token
			const refreshTokenData = await RefreshToken.findOne({ refreshId: verified.refreshId });
			
			if (!refreshTokenData || (new Date(refreshTokenData.expire) <= new Date(Date.now()))) {
				return res.status(400).send('Token not found or expired')
			}

			//check exist user
			const user = await User.findOne({ _id: refreshTokenData.userId });
			if (!user) {
				return res.status(400).send('User not found')
			}

			// create and assign token
			const { refreshToken, accessToken, refreshId } = tokenManager.generate(user._id);

			const saveRefreshToken = new RefreshToken({
				refreshId: refreshId,
				userId: user._id,
				expire: Date.now() + (60 * 60 * 24 * 30 * 1000)
			});

			try {
				await saveRefreshToken.save();
			} catch (err) {
				res.status(400).send(err);
			};

			const response = {
				id: user._id,
				name: user.name,
				lastName: user.lastName,
				email: user.email,
				role: user.role,
			}

			res.cookie('access_token', accessToken, { maxAge: (60 * 20 * 1000), httpOnly: true });
			res.cookie('refresh_token', refreshToken, { maxAge: (60 * 60 * 24 * 30 * 1000), httpOnly: true });

			log.info(`Refresh - Refresh Token exist`);
			res.send(response);

		} catch (err) {
			log.err(err);
			res.status(400).send('Invalid Token');
		}
	}
});

router.post('/login', async (req, res) => {
	//validation
	const { error } = loginValidation(req.body);

	if (error) {
		res.status(400).send(error.details[0].message)
	};

	//check user exist
	const user = await User.findOne({ email: req.body.email });
	if (!user) {
		return res.status(400).send('email not found')
	}

	//check password
	const validPass = await bcrypt.compare(req.body.password, user.password);
	if (!validPass) {
		return res.status(400).send('bad pass');
	}

	// create and assign token
	const { refreshToken, accessToken, refreshId } = tokenManager.generate(user._id);

	const saveRefreshToken = new RefreshToken({
		refreshId: refreshId,
		userId: user._id,
		expire: Date.now() + (60 * 60 * 24 * 30 * 1000)
	});

	try {
		await saveRefreshToken.save();
	} catch (err) {
		res.status(400).send(err);
	};

	const response = { 
		id: user._id, 
		name: user.name, 
		email: user.email, 
		role: user.role,
	}

	res.cookie('access_token', accessToken, { maxAge: (60 * 20 * 1000), httpOnly: true });
	res.cookie('refresh_token', refreshToken, { maxAge: (60 * 60 * 24 * 30 * 1000), httpOnly: true });

	log.info(`Login - OK`);
	res.send(response);
});

router.post('/logout', async (_, res) => {
	log.info(`Logout - coming soon`);
	res.status(200).send(`Logout - coming soon`);
});

module.exports = router;